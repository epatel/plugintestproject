#import <AppKit/AppKit.h>
#import "PluginProtocol.h"

@interface PTPContent1 : NSObject <PluginProtocol>

@property (nonatomic, weak) IBOutlet NSView *loadedView;

- (IBAction)buttonPressed:(id)sender;

@end
