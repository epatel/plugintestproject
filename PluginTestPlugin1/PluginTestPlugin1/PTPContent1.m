#import "PTPContent1.h"

@implementation PTPContent1

+ (void)initialize
{
    NSLog(@"initialize %@", self);
}

- (instancetype)init
{
    if ((self = [super init])) {
    }
    return self;
}

- (NSView*)view
{
    NSBundle *bundle = [NSBundle bundleForClass:[self class]];
    if (bundle) {
        NSArray *array;
        if ([bundle loadNibNamed:@"PTP1View" owner:self topLevelObjects:&array]) {
            return self.loadedView;
        }
    }
    return nil;
}

- (IBAction)buttonPressed:(id)sender
{
    NSLog(@"buttonPressed:");
}

@end
