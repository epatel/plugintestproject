#import "ViewController.h"
#import "PluginProtocol.h"

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self loadAllBundles];
}

- (void)setRepresentedObject:(id)representedObject
{
    [super setRepresentedObject:representedObject];
}

- (void)loadAllBundles
{
    _plugins = [NSMutableArray array];
    for (NSString *path in [[NSFileManager defaultManager] contentsOfDirectoryAtPath:[[NSBundle mainBundle] resourcePath]
                                                                               error:nil]) {
        if ([[path pathExtension] isEqualToString:@"bundle"]) {
            NSBundle *bundle = [NSBundle bundleWithPath:path];
            Class bundleClass = [bundle principalClass];
            if (bundleClass) {
                id<PluginProtocol> instance = [[bundleClass alloc] init];
                if (instance && [instance conformsToProtocol:@protocol(PluginProtocol)]) {
                    [_plugins addObject:instance]; // The plugin 'facad' need to be retained somewhere
                    NSView *view = [instance view];
                    [self.view addSubview:view];
                    // This part I am not sure why it is needed. Setting constraints explicity
                    NSDictionary *viewsDictionary = NSDictionaryOfVariableBindings(view);
                    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|-0-[view]-0-|"
                                                                                      options:0
                                                                                      metrics:nil
                                                                                        views:viewsDictionary]];
                    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[view]-0-|"
                                                                                      options:0
                                                                                      metrics:nil
                                                                                        views:viewsDictionary]];
                }
            }
        }
    }
}

@end
