#import <AppKit/AppKit.h>

@protocol PluginProtocol <NSObject>

- (NSView*)view;

@end
